/* Copyright (C) Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <getopt.h>
#include <errno.h>
#include <sys/time.h>
#include <linux/types.h>

#include "fish.h"
#include "ocean.h"

extern int errno;

/**
 * These options are read from command line args.
 */
int config_verbose;
int config_break;
__u64 config_weeks;

/**
 * These options can be changed using config file.
 */
static __u64 n	= 512;		/* grid size */
static __u64 fb	= 20;		/* breeding age of fish */
static __u64 sb	= 30;		/* breeding age of sharks */
static __u64 ss	= 25;		/* starvation time of sharks */
static __u64 fo	= 30;		/* old age when fish die */
static __u64 so	= 40;		/* old age when sharks die */
static __u64 fn	= 1000;		/* initial number of fishes */
static __u64 sn	= 400;		/* initial number of sharks */
static __u64 nt	= 64;		/* number of threads to be used */
static __u64 wk = 1000;		/* number of weeks to simulate */

static char *config_file;
static char linebuf[128];

static char *config_strings[] = {
	"n", "fb", "sb", "ss", "fo", "so", "fn", "sn", "nt", "wk"
};
static __u64 *config_vars[] = {
	&n, &fb, &sb, &ss, &fo, &so, &fn, &sn, &nt, &wk
};

static int read_config_file(void)
{
	int i;
	int res = 0;
	int lcount = 0;
	FILE *fp = fopen(config_file, "r");
	char *token;

	if (!fp) {
		perror("Failed to open config file");
		return errno;
	}

	n = fb = sb = ss = fo = so = fn = sn = nt = wk = 0;

	while (fgets(linebuf, 127, fp) != NULL) {
		lcount++;

		if (linebuf[0] == '#' || isspace(linebuf[0]))
			continue;

		linebuf[strlen(linebuf) - 1] = '\0';

		token = strtok(linebuf, " \t\n=");

		for (i = 0; i < sizeof(config_strings) / sizeof(char *); i++) {
			if (!strcmp(config_strings[i], token)) {
				token = strtok(NULL, " \t=");
				if (token == NULL) {
					fprintf(stderr,
						"Failed to parse line: %d",
						lcount);
					res = -EINVAL;
					goto out;
				}

				*config_vars[i] = (__u64) atoll(token);
				break;
			}
		}

		if (i == sizeof(config_strings) / sizeof(char *)) {
			fprintf(stderr, "Unknown argument: %s\n", token);
			res = -EINVAL;
			goto out;
		}
	}
	if (ferror(fp)) {
		perror("Error while reading the file");
		return errno;
	}

	if (!n || !fb || !sb || !ss || !fo || !so || !fn || !sn || !nt) {
		fprintf(stderr, "You cannot omit options if you use "
				"configuration file!\n");
		res = -EINVAL;
		goto out;
	}

out:
	fclose(fp);
	return res;
}

static inline int is_power_of_two(__u64 x)
{
	return ((x != 0) && ((x & (~x + 1)) == x));
}

static inline int is_perfect_square(__u64 x)
{
	double d_sqrt = sqrt((double) x);
	return (d_sqrt == (__u64) d_sqrt);
}

static int check_configs(void)
{
	int i;

	if (!is_power_of_two(n)) {
		fprintf(stderr, "n(%llu) should be power of two.\n", n);
		return -EINVAL;
	}

	if (fb >= fo) {
		fprintf(stderr, "fo(%llu) should be greater than fb(%llu).\n",
				fo, fb);
		return -EINVAL;
	}

	if (sb >= so) {
		fprintf(stderr, "so(%llu) should be greater than sb(%llu).\n",
				so, sb);
		return -EINVAL;
	}

	if (ss >= so) {
		fprintf(stderr, "so(%llu) should be greater than ss(%llu).\n",
				so, ss);
		return -EINVAL;
	}

	if (!is_power_of_two(nt)) {
		fprintf(stderr, "nt(%llu) should be power of two.\n", nt);
		return -EINVAL;
	}
	if (!is_perfect_square(nt)) {
		fprintf(stderr, "nt(%llu) should be perfect square.\n", nt);
		return -EINVAL;
	}

	if (nt > n*n) {
		fprintf(stderr, "nt(%llu) should be less than n*n(%llu).\n",
				nt, n*n);
		return -EINVAL;
	}

	config_weeks = wk;

	if (!config_verbose)
		return 0;

	printf("== Simulation Configuration ==\n");
	for (i = 0; i < sizeof(config_strings) / sizeof(char *); i++)
		printf("%s = %llu\n", config_strings[i], *config_vars[i]);

	return 0;
}

static void print_usage(char *exe)
{
	fprintf(stderr,
		"\nUsage: %s [-h] [-v] [-b] [-f file]\n\n"
		" -h         Shows this help message\n"
		" -v         Print verbose output\n"
		" -b         Insert break between weeks (only work with -v)\n"
		" -f <file>  Specify the configuration file\n\n"
		"Find the configuration file example in test.conf.\n\n"
		, exe);
}

int main(int argc, char **argv)
{
	int res = 0;
	int opt;
	__u64 i;
	struct ocean *ocean;
	struct fish *tmp;
	struct timeval initial, setup, final;

	/** TODO: all configuration params should be read using getopt from
	 * command line arguments */
	while ((opt = getopt(argc, argv, "hvbf:")) != -1) {
		switch (opt) {
		case 'v':
			config_verbose = 1;
			break;
		case 'b':
			config_break = 1;
			break;
		case 'f':
			config_file = optarg;
			break;
		case 'h':
		default:
			print_usage(argv[0]);
			return 0;
		}
	}

	/** Check whether all parameters make sense */
	if (config_file)
		if (read_config_file() < 0) {
			fprintf(stderr, "Failed to parse config file.\n");
			return -1;
		}

	if (check_configs() < 0) {
		fprintf(stderr, "Configuration not correct.. \n");
		res = -EINVAL;
		goto out;
	}

	/** Initialize the simulation */
	ocean = ocean_init(n);
	if (ocean == NULL) {
		res = -ENOMEM;
		goto out;
	}

	if (config_verbose)
		fprintf(stderr, "creating/adding %llu fishes..\n", fn);

	gettimeofday(&initial, NULL);

	for (i = 0; i < fn; i++) {
		__u64 pos;
		tmp = fish_init(fb, fo);

		res = ocean_add_fish(ocean, tmp, &pos);
		if (res < 0)
			goto out_fish;

		if (config_verbose)
			fprintf(stderr, "parcel[%llu]\t= fish (%p)\n", pos, tmp);
	}
	if (config_verbose)
		fprintf(stderr, "creating/adding %llu sharks..\n", sn);

	for (i = 0; i < sn; i++) {
		__u64 pos;
		tmp = shark_init(sb, so, ss);

		res = ocean_add_fish(ocean, tmp, &pos);
		if (res < 0)
			goto out_fish;
		if (config_verbose)
			fprintf(stderr, "parcel[%llu]\t= shark (%p)\n", pos, tmp);
	}

	/** Run the simulation.. */
	gettimeofday(&setup, NULL);

	res = ocean_move_on(ocean, nt);

	gettimeofday(&final, NULL);

	if (res < 0)
		fputs("Simulation failed!\n", stderr);
	else {
		__u64 init_time;
		__u64 running_time;

		init_time = (setup.tv_sec - initial.tv_sec) * 1000000;
		init_time += setup.tv_usec - initial.tv_usec;
		running_time = (final.tv_sec - setup.tv_sec) * 1000000;
		running_time += final.tv_usec - setup.tv_usec;

#if 0
		/** Instead of this, use the terse output.. */
		printf("\n== Simulation Result ==\n\n"
		       "Total time = %llu\n"
		       "  + initialization time = %llu\n"
		       "  + running time        = %llu\n\n",
		       init_time + running_time,
		       init_time, running_time);
#endif

		printf("%llu\t%llu\t%llu\n",
			init_time + running_time,
			init_time, running_time);
	}


out_fish:
	ocean_exit(ocean);
out:
	return res;
}

