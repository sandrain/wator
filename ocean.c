/* Copyright (C) Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>
#include <linux/types.h>

#include "fish.h"
#include "ocean.h"

static __u64 alive = 1;

/**
 * Thread work data.
 */
struct work_data {
	__u64 id;		/* ID of the thread */
	__u64 nparcels;		/* Parcels this thread is responsible for */
	struct ocean *ocean;	/* The ocean object */
	struct parcel **parcels; /* Block of parcels */
};

static pthread_barrier_t barrier;

/**
 * To determine the random position when adding fishes, we maintain shuffled
 * array and position.
 */
static __u64 *rand_sequence;
static __u64 current_sequence;

/**
 * If this is non-zero, it represents how many weeks we have to simulate. If
 * it's zero, we continue the simulation until no fish is left.
 */
static int times_up;

/**
 * array_shuffle, the function is borrowed from Nomadiq on stackoverflow. You
 * can find the original implementation in:
 * http://stackoverflow.com/questions/6127503/shuffle-array-in-c
 *
 * @array  The array to be shuffled.
 * @n      The size of @array.
 */
static void array_shuffle(__u64 *array, __u64 n)
{
	__u64 i, j, tmp;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	srand48(tv.tv_usec);

	if (n > 1) {
		for (i = n - 1; i > 0; i--) {
			j = (__u64) (drand48() * (i + 1));
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
		}
	}
}

/**
 * Initialize the random sequence (used when adding fishes).
 *
 * @size  Number of sequences to be generated.
 */
static void init_rand_sequence(__u64 size)
{
	__u64 i;

	for (i = 0; i < size; i++)
		rand_sequence[i] = i;

	array_shuffle(rand_sequence, size);
}

/**
 * Find a free slot for newly coming family.
 *
 * @self  The ocean instance.
 * @pos   [out] The posision determined.
 *
 * Returns 0 if free slots are found. -1 if not.
 */
static inline int find_free_parcel(struct ocean *self, __u64 *pos)
{
	if (current_sequence++ == self->n * self->n)
		return -1;

	*pos = rand_sequence[current_sequence++];
	return 0;
}

/* FIXME: UGLY implementation. Any better way to do this?? */
/**
 * Allocates parcels (sub-matrix) to the thread. The parcels will be set in
 * @data.
 *
 * @self  The ocean instance.
 * @nthreads Total number of threads spawned.
 * @data  [out] The thread work data.
 */
static void set_thread_parcels(struct ocean *self, __u64 nthreads,
				struct work_data *data)
{
	__u64 i, j, k, np, snp, ss, n, pos, id;

	id = data->id;
	n = self->n;
	np = n * n / nthreads;
	snp = (__u64) sqrt(np);
	ss = n / snp;

	k = 0;

	for (i = 0; i < snp; i++) {
		pos = (id/ss)*snp*n + (id%ss)*snp + i*n;
		for (j = pos; j < pos + snp; j++)
			data->parcels[k++] = &self->parcels[j];
	}
}

/* FIXME: UGLY implementation. Any better way to do this?? */
/**
 * Set neighbors for given parcel (cell).
 *
 * @parcel  The parcel instance.
 * @ocean   The ocean instance.
 */
static void parcel_set_neighbors(struct parcel *parcel, struct ocean *ocean)
{
	int i = 0;
	__u64 n = ocean->n;
	__u64 pos = parcel->position;
	__u64 npos[8];

	npos[i++] = pos - n - 1;
	npos[i++] = pos - n;
	npos[i++] = pos - n + 1;
	npos[i++] = pos - 1;
	npos[i++] = pos + 1;
	npos[i++] = pos + n - 1;
	npos[i++] = pos + n;
	npos[i]   = pos + n + 1;

	/* adjust vertical indices */
	if (pos < n) {
		for (i = 0; i < 3; i++)
			npos[i] += n*n;
	}
	else if (pos >= n * (n - 1)) {
		for (i = 5; i < 8; i++)
			npos[i] -= n*n;
	}

	/* adjust horizontal indices */
	if (pos % n == 0) {
		npos[0] += n;
		npos[3] += n;
		npos[5] += n;
	}
	else if ((pos + 1) % n == 0) {
		npos[2] -= n;
		npos[4] -= n;
		npos[7] -= n;
	}

	if (pos == 0)
		npos[0] = n*n - 1;
	else if (pos == n - 1)
		npos[2] = n*(n - 1);
	else if (pos == n*(n - 1))
		npos[5] = n - 1;
	else if (pos == n*n - 1)
		npos[7] = 0;

	for (i = 0; i < 8; i++)
		parcel->neighbors[i] = &ocean->parcels[npos[i]];
}

/**
 * Take a random parcel among the neighbor. If the parcel is already occupied
 * by other, than just gives up. This function is used when fish tries to move
 * to its neighbor.
 *
 * @parcel  The parcel instance.
 *
 * Returns an available parcel on success, otherwise returns NULL.
 *
 * NOTE: This function returns a parcel with its lock acquired. Caller should
 * unlock it.
 */
static struct parcel *get_random_neighbor(struct parcel *parcel)
{
	int res;
	int pos = (int) drand48() * 8;
	struct parcel *neighbor = parcel->neighbors[pos];

	res = pthread_mutex_trylock(&neighbor->lock);
	if (res)
		return NULL;

	return neighbor;
}

/**
 * Find an edible fish among my neighbors. If found returns that parcel. If
 * not, just returns one of empty parcel.
 *
 * @parcel The parcel instance.
 *
 * Returns an edible/available parcel on success, otherwise returns NULL.
 * 
 * NOTE: This function returns a parcel with its lock acquired. Caller should
 * unlock it.
 */
static struct parcel *get_edible_neighbor(struct parcel *parcel)
{
	int res, i;
	struct parcel *neighbor;

	/* Find a fish first */
	for (i = 0; i < 8; i++) {
		neighbor = parcel->neighbors[i];
		res = pthread_mutex_trylock(&neighbor->lock);
		if (res)
			continue;

		if (neighbor->fish && neighbor->fish->type == TYPE_FISH)
			return neighbor;

		pthread_mutex_unlock(&neighbor->lock);
	}

	/* No fish available, find an empty neighbor */
	for (i = 0; i < 8; i++) {
		neighbor = parcel->neighbors[i];
		res = pthread_mutex_trylock(&neighbor->lock);
		if (res)
			continue;
		if (!neighbor->fish)
			return neighbor;
		pthread_mutex_unlock(&neighbor->lock);
	}

	return NULL;
}

/**
 * The thread function, which does the main simulation works.
 *
 * @arg  The thread argument.
 *
 * Returns NULL.
 */
static void *ocean_work(void *arg)
{
	int res;
	__u64 i;
	struct work_data *data = (struct work_data *) arg;
	struct parcel *parcel, *togo = NULL;
	struct fish *fish;
	struct shark_starvation *st;

	while (1) {
		pthread_barrier_wait(&barrier);

		if (alive == 0 || times_up)
			break;

		for (i = 0; i < data->nparcels; i++) {
			parcel = data->parcels[i];
			res = pthread_mutex_trylock(&parcel->lock);
			if (res)
				/* failed to acquire lock.. maybe caught by
				 * shark */
				continue;

			fish = parcel->fish;

			if (!fish) {
				pthread_mutex_unlock(&parcel->lock);
				continue;
			}

			if (fish->type > 1) {
				if (config_verbose) {
					printf("corrupted data [%llu]: "
						"%p, b=%u, l=%u\n",
						parcel->position, fish,
						fish->breeding_age,
						fish->lifespan);
				}
				(void) getchar();
				pthread_mutex_unlock(&parcel->lock);
				continue;
			}

			if (fish->moved) {
				pthread_mutex_unlock(&parcel->lock);
				continue;
			}

			fish->age++;

			if (fish->age == fish->lifespan) {
				fish_exit(fish);
				parcel->fish = NULL;
				pthread_mutex_unlock(&parcel->lock);
				if (config_verbose) {
					printf("parcel[%llu] %s: die (old)\n",
						parcel->position,
						fish->type ? "shark" : "fish");
				}
				continue;
			}

			if (fish->type == TYPE_FISH) {
				togo = get_random_neighbor(parcel);
				if (togo == NULL || togo->fish) {
					if (togo)
						pthread_mutex_unlock(&togo->lock);
					pthread_mutex_unlock(&parcel->lock);
					if (config_verbose) {
						printf("parcel[%llu] fish: "
							"stay\n",
							parcel->position);
					}
					continue; /* nowhere to move */
				}
			}
			else if (fish->type == TYPE_SHARK) { /* TYPE_SHARK */
				st = (struct shark_starvation *) fish->private;

				togo = get_edible_neighbor(parcel);
				if (togo == NULL) {
					pthread_mutex_unlock(&parcel->lock);
					if (config_verbose) {
						printf("parcel[%llu] shark: "
							"stay\n",
							parcel->position);
					}
					continue;
				}

				if (togo->fish) {
					if (config_verbose) {
						printf("parcel[%llu] shark: "
							"eatup at %llu (%p)\n",
							parcel->position,
							togo->position,
							togo->fish);
					}
					fish_exit(togo->fish); /* eat up */
					togo->fish = NULL;
					st->current = 0;
				}
				else {
					if (++st->current == st->max) {
						fish_exit(fish);
						parcel->fish = NULL;

						pthread_mutex_unlock(
								&togo->lock);
						pthread_mutex_unlock(
								&parcel->lock);

						if (config_verbose) {
							printf("parcel[%llu] "
							      "shark: "
							      "die (starved)\n",
							      parcel->position);
						}
						continue;
					}
				}
			}
			else {
			}

			togo->fish = fish;
			fish->moved = 1;

			if (config_verbose) {
				printf("parcel[%llu] %s: moved to %llu\n",
					parcel->position,
					fish->type ? "shark" : "fish",
					togo->position);
			}

			if (fish->age >= fish->breeding_age && fish->mature == 0) {
				/* TODO: what if clone fails?? */
				parcel->fish = fish_clone(fish);
				fish->mature = 1;
				if (config_verbose) {
					printf("new %s at %llu\n",
					       parcel->fish->type ?
					       "shark" : "fish",
					       parcel->position);
				}
			}
			else
				parcel->fish = NULL;

			pthread_mutex_unlock(&togo->lock);
			pthread_mutex_unlock(&parcel->lock);
		}

		pthread_barrier_wait(&barrier);

		for (i = 0; i < data->nparcels; i++) {
			parcel = data->parcels[i];
			if (parcel->fish)
				parcel->fish->moved = 0;
		}
	}

	pthread_exit(0);
}

/**
 * See the ocean.h for external interfaces.
 */

struct ocean *ocean_init(__u64 n)
{
	struct ocean *self = malloc(sizeof(*self)
				  + sizeof(struct parcel) * n * n
				  + sizeof(__u64) * n * n);

	if (self) {
		__u64 i;

		self->n = n;
		self->time = 0;

		for (i = 0; i < n * n; i++) {
			struct parcel *current = &self->parcels[i];
			current->position = i;
			current->fish = NULL;
			pthread_mutex_init(&current->lock, NULL);

			parcel_set_neighbors(current, self);
		}

		rand_sequence = (__u64 *) &self->parcels[n*n];
		init_rand_sequence(n*n);
	}

	return self;
}

void ocean_exit(struct ocean *self)
{
	if (self) {
		__u64 i, len = self->n * self->n;
		struct parcel *current;

		for (i = 0; i < len; i++) {
			current = &self->parcels[i];
			if (current->fish)
				fish_exit(current->fish);
			pthread_mutex_destroy(&current->lock);
		}
		free(self);
	}
}

int ocean_add_fish(struct ocean *self, struct fish *fish, __u64 *position)
{
	__u64 pos;
	int res;
	struct parcel *parcel;

	res = find_free_parcel(self, &pos);
	if (res < 0)
		return res;

	if (self->parcels[pos].fish != NULL)
		return -1;

	parcel = &self->parcels[pos];
	parcel->fish = fish;
	*position = pos;

	return 0;
}

int ocean_move_on(struct ocean *self, __u64 nthreads)
{
	int res = 0;
	__u64 nt, np;
	pthread_t *threads;
	struct work_data *data, *current;
	struct parcel **parcels;
	size_t memsize = (sizeof(*threads) + sizeof(*data)) * nthreads
			+ sizeof(struct parcel *) * self->n * self->n;

	threads = (pthread_t *) malloc(memsize);
	if (!threads)
		return -1;

	data = (struct work_data *) &threads[nthreads];
	parcels = (struct parcel **) &data[nthreads];
	memset(threads, 0, memsize);

	np = self->n * self->n / nthreads;

	pthread_barrier_init(&barrier, NULL, nthreads + 1);

	for (nt = 0; nt < nthreads; nt++) {
		current = &data[nt];

		current->id = nt;
		current->nparcels = np;
		current->ocean = self;
		current->parcels = &parcels[nt * current->nparcels];

		set_thread_parcels(self, nthreads, current);

		if (config_verbose) {
			__u64 i, nfish = 0, nshark = 0;
			struct parcel **d = current->parcels;
			printf("thread[%llu]\n + {", nt);
			for (i = 0; i < current->nparcels; i++) {
				printf(" %llu%c", d[i]->position,
				       i == current->nparcels - 1 ?  ' ' : ',');
				if (d[i]->fish) {
					if (d[i]->fish->type == TYPE_FISH)
						nfish++;
					else
						nshark++;
				}
			}
			printf("}\n + %llu fishes, %llu sharks\n",
			       nfish, nshark);
		}

		res = pthread_create(&threads[nt], NULL, ocean_work, current);
		if (res < 0) {
			while (--nt)
				pthread_kill(threads[nt], SIGKILL);
			goto out;
		}
	}

	while (1) {
		pthread_barrier_wait(&barrier);
		if (alive == 0 || times_up)
			break;
		/* Let workers run.. */
		pthread_barrier_wait(&barrier);

		self->time++;


		if (config_verbose) {
			__u64 i, total = self->n * self->n;

			printf("\n== week %llu ==\n", self->time);

			alive = 0;
			for (i = 0; i < total; i++) {
				struct parcel *tmp = &self->parcels[i];

				if (!tmp->fish)
					continue;

				if (tmp->fish->type == TYPE_SHARK) {
					struct shark_starvation *st;
					st = (struct shark_starvation *)
						tmp->fish->private;
					printf("%3llu (%3llu, %3llu),\tshark "
					       "(%p):\tage = %u, starved = %u\n",
					       tmp->position,
					       tmp->position / self->n,
					       tmp->position % self->n,
					       tmp->fish,
					       tmp->fish->age, st->current);
				}
				else {
					printf("%3llu (%3llu, %3llu),\tfish  "
					       "(%p):\tage = %u\n",
					       tmp->position,
					       tmp->position / self->n,
					       tmp->position % self->n,
					       tmp->fish,
					       tmp->fish->age);
				}
				alive++;
			}

			if (config_verbose)
				printf("%llu fishes are alive\n\n", alive);
		}

		if (config_verbose && config_break) {
			fputs("Press ENTER to continue..", stdout);
			fflush(stdout);
			(void) getchar();
		}

		if (config_weeks == self->time)
			times_up = 1;
	}
out:
	for (nt = 0; nt < nthreads; nt++)
		pthread_join(threads[nt], (void **) NULL);

	pthread_barrier_destroy(&barrier);
	free(threads);

	fish_cleanup_ghost();

	return res;
}

