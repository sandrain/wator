/* Copyright (C) Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "fish.h"

/**
 * These ghost lists are used to hold freed fish objects. This can alleviate
 * the overhead of frequent malloc().
 */
static struct fish *ghost_fish;
static pthread_mutex_t ghost_fish_lock = PTHREAD_MUTEX_INITIALIZER;

static struct fish *ghost_shark;
static pthread_mutex_t ghost_shark_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 * Get the available ghost.
 *
 * @type  TYPE_FISH | TYPE_SHARK
 *
 * Returns a pointer to an space if available. NULL otherwise.
 */
static struct fish *get_ghost(int type)
{
	struct fish *ghost = NULL;

	switch (type) {
	case TYPE_FISH:
		pthread_mutex_lock(&ghost_fish_lock);
		ghost = ghost_fish;
		if (ghost)
			ghost_fish = ghost->next;
		pthread_mutex_unlock(&ghost_fish_lock);
		break;
	case TYPE_SHARK:
		pthread_mutex_lock(&ghost_shark_lock);
		ghost = ghost_shark;
		if (ghost)
			ghost_shark = ghost->next;
		pthread_mutex_unlock(&ghost_shark_lock);
		break;
	default:
		break;
	}

	return ghost;
}

/**
 * Initialize fish object whose space is already allocated by the caller.
 *
 * @self  The fish object.
 * @type  The type of this fish (fish or shark)?
 * @breeding_age  The breeding age of this fish.
 * @lifespan  The lifespan of this fish.
 *
 * Always returns initialized fish object.
 */
static inline struct fish *fish_init_allocated(struct fish *self,
				int type, __u32 breeding_age, __u32 lifespan)
{
	memset(self, 0, sizeof(*self));
	self->next = NULL;
	self->type = type;
	self->age = 0;
	self->breeding_age = breeding_age;
	self->lifespan = lifespan;
	self->moved = 0;

	return self;
}

/**
 * See the fish.h for external interfaces.
 */

struct fish *fish_init(__u32 breeding_age, __u32 lifespan)
{
	struct fish *self;

	self = get_ghost(TYPE_FISH);
	if (!self)
		self = malloc(sizeof(*self));

	if (self)
		fish_init_allocated(self, TYPE_FISH, breeding_age, lifespan);

	return self;
}

void fish_exit(struct fish *self)
{
	if (self) {
		if (self->type == TYPE_FISH) {
			pthread_mutex_lock(&ghost_fish_lock);
			self->next = ghost_fish;
			ghost_fish = self;
			pthread_mutex_unlock(&ghost_fish_lock);
		}
		else if (self->type == TYPE_SHARK) {
			pthread_mutex_lock(&ghost_shark_lock);
			self->next = ghost_shark;
			ghost_shark = self;
			pthread_mutex_unlock(&ghost_shark_lock);
		}
	}
}

struct fish *shark_init(__u32 breeding_age, __u32 lifespan,
			__u32 max_starvation)
{
	struct fish *self;
	struct shark_starvation *st;

	self = get_ghost(TYPE_SHARK);
	if (!self)
		self = malloc(sizeof(*self) + sizeof(*st));

	if (!self)
		return NULL;

	fish_init_allocated(self, TYPE_SHARK, breeding_age, lifespan);
	st = (struct shark_starvation *) &self[1];
	st->current = 0;
	st->max = max_starvation;
	self->private = st;

	return self;
}

struct fish *fish_clone(struct fish *self)
{
	struct fish *babyfish;
	struct shark_starvation *st, *tmp;
	size_t size = sizeof(*babyfish);

	if (self->type == TYPE_SHARK)
		size += sizeof(struct shark_starvation);

	babyfish = get_ghost(self->type);
	if (!babyfish)
		babyfish = malloc(size);

	if (!babyfish)
		return NULL;

	babyfish->type = self->type;
	babyfish->breeding_age = self->breeding_age;
	babyfish->lifespan = self->lifespan;
	babyfish->age = 0;
	babyfish->moved = 0;
	babyfish->mature = 0;

	if (self->type == TYPE_SHARK) {
		tmp = (struct shark_starvation *) self->private;
		st = (struct shark_starvation *) &babyfish[1];
		st->current = 0;
		st->max = tmp->max;

		babyfish->private = st;
	}

	return babyfish;
}

void fish_cleanup_ghost(void)
{
	struct fish *tmp;

	while ((tmp = ghost_fish) != NULL) {
		ghost_fish = tmp->next;
		free(tmp);
	}

	while ((tmp = ghost_shark) != NULL) {
		ghost_shark = tmp->next;
		free(tmp);
	}
}

