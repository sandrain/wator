#
# Makefile

CC	= gcc
CFLAGS	= -O2 -Wall #-ggdb -O0
LDFLAGS	= -lm -lpthread

TARGET	= wator
OBJS	= fish.o ocean.o wator.o

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

all: $(TARGET)

clean:
	rm -f *.o $(TARGET)


