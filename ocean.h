/* Copyright (C) Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#ifndef	__OCEAN_H__
#define __OCEAN_H__

#include <linux/types.h>
#include <pthread.h>

struct fish;

/**
 * The ocean is represented as a matrix (see below). Each cell of the matrix is
 * represented as 'parcel'.
 */
struct parcel {
	__u64 position;			/* Position inside the matrix */
	struct fish *fish;		/* Fish currently staying in */
	struct parcel *neighbors[8];	/* Neighbor cells */
	pthread_mutex_t lock;		/* Mutex for accessing fish */
};

/**
 * The ocean structure, actually represents 2D matrix but implemented using 1D
 * array.
 */
struct ocean {
	__u64	n;			/* n by n matrix */
	__u64	time;			/* Simulation time */
	struct parcel parcels[0];	/* Matrix cells */
};

/**
 * ocean_init initialize ocean object.
 *
 * @n  Size of ocean. (n by n matrix)
 *
 * Returns initialized object, which should be freed by ocean_exit(). Returns
 * NULL on failure.
 */
struct ocean *ocean_init(__u64 n);

/**
 * ocean_exit frees up the ocean object.
 *
 * @self ocean object to be freed.
 */
void ocean_exit(struct ocean *self);

/**
 * ocean_add_fish adds fish to the ocean. The parcel where the fish is placed
 * is chosen randomly.
 *
 * @self  The ocean instance.
 * @fish  The fish object to be added.
 * @position [out] The position determined randomly.
 *
 * Returns 0 on success, otherwise <0.
 */
int ocean_add_fish(struct ocean *self, struct fish *fish, __u64 *position);

/**
 * ocean_move_on starts the simulation. Before calling this function the caller
 * has to initialize all the simulation environments.
 *
 * @self  The ocean instance.
 * @nthreads Number of threads to be used.
 *
 * Return 0 on success, otherwise <0.
 */
int ocean_move_on(struct ocean *self, __u64 nthreads);

/**
 * These variables are defined in main driver (wator.c).
 * [FIXME] They have to be passed to the ocean_init().
 */
extern int config_verbose;
extern int config_break;
extern __u64 config_weeks;

#endif	/* __OCEAN_H__ */

