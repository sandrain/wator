/* Copyright (C) Hyogi Sim <hyogi@cs.vt.edu>
 * 
 * ---------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#ifndef	__FISH_H__
#define	__FISH_H__

#include <linux/types.h>

struct ocean;

struct fish {
	struct fish *next;	/* For ghost list */

#define	TYPE_FISH	0
#define	TYPE_SHARK	1

	int	type;		/* Fish/Shark ? */
	__u32	age;		/* Current age of fish */
	__u32	breeding_age;	/* Breeding age of fish */
	__u32	lifespan;	/* Lifespan of fish */

	int	moved;		/* Is this fish already moved in this turn? */
	int	mature;		/* Is this fish already has a child? */

	void	*private;	/* Reserved for shark */
};

struct shark_starvation {
	__u32 current;		/* Current starvation */
	__u32 max;		/* Maximum starvation */
};

/**
 * fish_init creates an instance of fish.
 *
 * @breeding_age  The age this fish breeds.
 * @lifespan      The lifespan of this fish.
 *
 * Return fish object, which should be destroyed by fish_exit(). On failure,
 * this returns NULL.
 */
struct fish *fish_init(__u32 breeding_age, __u32 lifespan);

/**
 * fish_exit destroys the fish object.
 *
 * @self  The fish object to be destroyed.
 */
void fish_exit(struct fish *self);

/**
 * shark_init creates an instance of shark.
 *
 * @breeding_age  The age this shark breeds.
 * @lifespan      The lifespan of this shark.
 * @max_starvation The maximum starvation time.
 *
 * Returns shark object, which should be destroyed by fish_exit(). On failure,
 * this returns NULL.
 */
struct fish *shark_init(__u32 breeding_age, __u32 lifespan,
			__u32 max_starvation);

/**
 * fish_clone clones fish and returns newly cloned fish.
 *
 * @fish  The fish should be cloned.
 *
 * Returns newly created fish object. On failure, returns NULL.
 */
struct fish *fish_clone(struct fish *fish);

/**
 * fish_cleanup_ghost [FIXME] This design is __UGLY__. To avoid frequently
 * calling malloc(), freed fish objects are maintained in a ghost list
 * internally.
 * This function frees up the list, probably when the caller invokes this after
 * finishing its simulation.
 */
void fish_cleanup_ghost(void);

#endif	/* __FISH_H__ */

